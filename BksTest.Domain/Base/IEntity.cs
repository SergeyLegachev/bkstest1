﻿using System;

namespace BksTest.Domain.Core.Base
{
    public interface IEntity : IEquatable<IEntity>
    {
        Guid Id { get; }
    }
}
