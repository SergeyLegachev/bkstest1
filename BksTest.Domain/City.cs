﻿using System.Collections.Generic;
using BksTest.Domain.Core.Base;

namespace BksTest.Domain.Core
{
    public class City : Entity
    {
        public string Name { get; set; }
        public virtual ICollection<FormData> FormDatas { get; set; } = new HashSet<FormData>();
    }
}
