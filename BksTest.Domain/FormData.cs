﻿using System;
using BksTest.Domain.Core.Base;

namespace BksTest.Domain.Core
{
    public class FormData : Entity
    {
        public string Fio { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public Guid CityId { get; set; }
        public virtual City City { get; set; }
    }
}
