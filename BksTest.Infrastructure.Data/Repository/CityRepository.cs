﻿using BksTest.Domain.Contracts;
using BksTest.Domain.Core;
using BksTest.Infrastructure.Data.Repository.Base;

namespace BksTest.Infrastructure.Data.Repository
{
    public class CityRepository : Repository<City>, ICityRepository
    {
        public CityRepository(IUnitOfWork uow) : base(uow)
        {
        }
    }
}
