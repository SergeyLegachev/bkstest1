﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BksTest.Domain.Contracts;
using BksTest.Domain.Core.Base;
using Microsoft.EntityFrameworkCore;

namespace BksTest.Infrastructure.Data.Repository.Base
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly IUnitOfWork _uow;

        public Repository(IUnitOfWork uow)
        {
            _uow = uow;
        }
        protected BksTestDbContext DbContext => _uow.Context;
        protected DbSet<TEntity> Set => DbContext.Set<TEntity>();

        public virtual IQueryable<TEntity> Query(bool disableTracking = true)
        {
            IQueryable<TEntity> query = Set.AsQueryable();

            if (disableTracking)
            {
                query = query.AsNoTracking();
            }

            return query;
        }

        public virtual Task<TEntity> GetByIdAsync(Guid id)
        {
            return Set.SingleAsync(e => e.Id == id);
        }

        public virtual TEntity Insert(TEntity entity)
        {
            TEntity elem = Set.Add(entity) as TEntity;
            return DbContext.Entry(entity).Entity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            AttachIfNot(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public virtual void Remove(Guid id)
        {
            var entity = Set.Find(id);

            if (entity == null)
            {
                return;
            }

            Remove(entity);
        }
        public virtual void Remove(TEntity entity)
        {
            AttachIfNot(entity);
            Set.Remove(entity);
        }
        protected virtual void AttachIfNot(TEntity entity)
        {
            if (!Set.Local.Contains(entity))
            {
                Set.Attach(entity);
            }
        }
    }
}
