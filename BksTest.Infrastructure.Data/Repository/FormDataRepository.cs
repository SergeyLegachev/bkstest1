﻿using BksTest.Domain.Contracts;
using BksTest.Domain.Core;
using BksTest.Infrastructure.Data.Repository.Base;

namespace BksTest.Infrastructure.Data.Repository
{
    public class FormDataRepository : Repository<FormData>, IFormDataRepository
    {
        public FormDataRepository(IUnitOfWork uow) : base(uow)
        {
        }
    }
}
