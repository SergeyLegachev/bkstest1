﻿using System.Threading.Tasks;

namespace BksTest.Infrastructure.Data
{
    public interface IUnitOfWork
    {
        BksTestDbContext Context { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
