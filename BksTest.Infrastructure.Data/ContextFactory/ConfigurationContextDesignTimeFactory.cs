﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace BksTest.Infrastructure.Data.ContextFactory
{
    public class ConfigurationContextDesignTimeFactory : IDesignTimeDbContextFactory<BksTestDbContext>
    {
        public BksTestDbContext CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<BksTestDbContext> builder = new DbContextOptionsBuilder<BksTestDbContext>();
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            builder.UseSqlServer(configuration.GetSection("ConnectionString").Value);
            return new BksTestDbContext(builder.Options);
        }
    }
}
