﻿using BksTest.Domain.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BksTest.Infrastructure.Data.Mapping
{
    public class FormDataConfiguration : IEntityTypeConfiguration<FormData>
    {
        internal const string TableName = "FormDatas";
        public void Configure(EntityTypeBuilder<FormData> builder)
        {
            builder.ToTable(TableName);
            builder.HasOne(e => e.City)
                .WithMany(e => e.FormDatas)
                .HasForeignKey(d => d.CityId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            builder.Property(e => e.Email).IsRequired();
            builder.Property(e => e.Fio).IsRequired();
            builder.Property(e => e.PhoneNumber).IsRequired();
            builder.Property(e => e.CityId).IsRequired();
        }
    }
}
