﻿using System;
using BksTest.Domain.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BksTest.Infrastructure.Data.Mapping
{
    public class CityConfiguration : IEntityTypeConfiguration<City>
    {
        internal const string TableName = "Cities";
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder.ToTable(TableName);
            builder.Property(e => e.Name).IsRequired();
        }
    }
}
