﻿using BksTest.Domain.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace BksTest.Infrastructure.Data
{
    public class DbInitialize
    {
        private readonly BksTestDbContext _context;

        public DbInitialize(BksTestDbContext context)
        {
            _context = context;
        }

        public void Initialize()
        {
            _context.Database.Migrate();
            if (!_context.Cities.Any())
            {
                City novosib = new City
                {
                    Name = "Новосибирск"
                };
                _context.Cities.Add(novosib);

                City krasnoyarsk = new City
                {
                    Name = "Красноярск"
                };
                _context.Cities.Add(krasnoyarsk);
                City moscow = new City
                {
                    Name = "Москва"
                };
                _context.Cities.Add(moscow);
                City leningrad = new City
                {
                    Name = "Санкт-Петербург"
                };
                _context.Cities.Add(leningrad);
                City kazan = new City
                {
                    Name = "Казань"
                };
                _context.Cities.Add(kazan);
                City ekb = new City
                {
                    Name = "Екатиренбург"
                };
                _context.Cities.Add(ekb);
                City rostov = new City
                {
                    Name = "Ростов-на-Дону"
                };
                _context.Cities.Add(rostov);
                City irkuts = new City
                {
                    Name = "Иркутск"
                };
                _context.Cities.Add(irkuts);
                City vladivosotok = new City
                {
                    Name = "Владивосток"
                };
                _context.Cities.Add(vladivosotok);
                _context.SaveChanges();
            }
        }
    }
}
