﻿using System;
using System.Threading.Tasks;
using BksTest.Domain.Contracts;

namespace BksTest.Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BksTestDbContext _context;
        public BksTestDbContext Context => _context;

        public UnitOfWork(BksTestDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await Context.SaveChangesAsync();
        }
    }
}
