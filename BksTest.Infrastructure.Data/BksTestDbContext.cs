﻿using BksTest.Domain.Contracts;
using BksTest.Domain.Core;
using BksTest.Infrastructure.Data.Mapping;
using Microsoft.EntityFrameworkCore;

namespace BksTest.Infrastructure.Data
{
    public class BksTestDbContext : DbContext
    {
        public BksTestDbContext(DbContextOptions options) : base(options)
        {

        }
        public virtual DbSet<FormData> FormDatas { get; set; }
        public virtual DbSet<City> Cities { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new FormDataConfiguration());
            modelBuilder.ApplyConfiguration(new CityConfiguration());
        }
    }
}
