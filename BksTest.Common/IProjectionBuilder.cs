﻿namespace BksTest.Common
{
    public interface IProjectionBuilder
    {
        TProjection Build<TValue, TProjection>(TValue value);
    }
}
