﻿using BksTest.Domain.Core;

namespace BksTest.Domain.Contracts
{
    public interface IFormDataRepository : IRepository<FormData>
    {
    }
}
