﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BksTest.Domain.Core.Base;

namespace BksTest.Domain.Contracts
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        IQueryable<TEntity> Query(bool disableTracking = true);
        Task<TEntity> GetByIdAsync(Guid id);
        TEntity Insert(TEntity entity);
        TEntity Update(TEntity entity);
        void Remove(Guid id);
        void Remove(TEntity entity);
    }
}
