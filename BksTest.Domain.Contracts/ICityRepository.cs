﻿using BksTest.Domain.Core;

namespace BksTest.Domain.Contracts
{
    public interface ICityRepository : IRepository<City>
    {
    }
}
