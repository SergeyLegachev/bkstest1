﻿using System.Collections.Generic;
using System.Linq;
using BksTest.Domain.Contracts;
using BksTest.Domain.Core;
using BksTest.Services.Contracts;

namespace BksTest.Infrastructure.BusinessLogic
{
    public class CityService : ICityService
    {
        private readonly ICityRepository _cityRepository;

        public CityService(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }

        public IEnumerable<City> GetAll()
        {
            return _cityRepository.Query().ToList();
        }
    }
}
