﻿using System.Threading.Tasks;
using BksTest.Domain.Contracts;
using BksTest.Domain.Core;
using BksTest.Infrastructure.Data;
using BksTest.Services.Contracts;

namespace BksTest.Infrastructure.BusinessLogic
{
    public class FormDataService : IFormDataService
    {
        private readonly IFormDataRepository _formDataRepository;
        private readonly IUnitOfWork _unitOfWork;

        public FormDataService(IFormDataRepository formDataRepository, IUnitOfWork unitOfWork)
        {
            _formDataRepository = formDataRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task Create(FormData editTaskDto)
        {
            _formDataRepository.Insert(editTaskDto);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
