﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using BksTest.Domain.Core;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace BksTest.Web.Models
{
    public class FormDataViewModel
    {
        public FormDataViewModel()
        {

        }
        public FormDataViewModel(IEnumerable<City> cities)
        {
            Fio = string.Empty;
            PhoneNumber = string.Empty;
            Email = string.Empty;
            CityId = cities.First().Id;
            Cities = cities.Select(e => new SelectListItem
            {
                Value = e.Id.ToString(),
                Text = e.Name
            });
        }
        [RegularExpression(@"^([а-яА-ЯёЁ])+(\s)+[а-яА-ЯёЁ]+(\s)+[а-яА-ЯёЁ]+$",
            ErrorMessage = "Проверьте, что введены кириллические символы и ФИО введено полностью")]
        [Required(ErrorMessage = "Обязательно для заполнения!")]
        public string Fio { get; set; }
        [RegularExpression(@"^\+7\(?([0-9]{3})\)?([0-9]{3}) +([0-9]{2}) +([0-9]{2})$",
            ErrorMessage = "Формат телефона +7(xxx)xxx xx xx !")]
        [Required(ErrorMessage = "Обязательно для заполнения!")]
        public string PhoneNumber { get; set; }
        [RegularExpression(@"(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                           + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                           + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                           + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})",
            ErrorMessage = "Некорректный формат")]
        [Required(ErrorMessage = "Обязательно для заполнения!")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Обязательно для заполнения!")]
        public Guid CityId { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }
    }
}
