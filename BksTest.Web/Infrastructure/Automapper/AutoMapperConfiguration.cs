﻿using System;
using System.Collections.Generic;
using AutoMapper;

namespace BksTest.Web.Infrastructure.Automapper
{
    public static class AutoMapperConfiguration
    {
        public static void Initialize()
        {
            Mapper.Initialize((cfg) =>
            {
                cfg.AddProfiles(GetAutoMapperProfiles());
            });
        }

        private static IEnumerable<Type> GetAutoMapperProfiles()
        {
            List<Type> result = new List<Type>();
            result.Add(typeof(WebProfile));
            return result;
        }
    }
}
