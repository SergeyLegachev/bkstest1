﻿using AutoMapper;
using BksTest.Domain.Core;
using BksTest.Web.Models;

namespace BksTest.Web.Infrastructure.Automapper
{
    public class WebProfile : Profile
    {
        public WebProfile()
        {
            CreateMap<FormDataViewModel, FormData>()
                .ForMember(e => e.City, opt => opt.Ignore())
                .ForMember(e => e.Id, opt => opt.Ignore());
        }
    }
}
