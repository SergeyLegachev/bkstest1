﻿using AutoMapper;
using BksTest.Common;

namespace BksTest.Web.Infrastructure.Automapper
{
    public class ProjectionBuilder : IProjectionBuilder
    {
        public TProjection Build<TValue, TProjection>(TValue value)
        {
            TProjection result = Mapper.Map<TValue, TProjection>(value);
            return result;
        }
    }
}
