﻿using AutoMapper;
using BksTest.Common;
using BksTest.Domain.Contracts;
using BksTest.Infrastructure.BusinessLogic;
using BksTest.Infrastructure.Data;
using BksTest.Infrastructure.Data.Repository;
using BksTest.Services.Contracts;
using BksTest.Web.Infrastructure.Automapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BksTest.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddDbContext<BksTestDbContext>(options =>
                options.UseSqlServer(Configuration.GetSection("ConnectionString").Value));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            AutoMapperConfiguration.Initialize();
            services.AddTransient<DbInitialize>();
            services.AddScoped<IProjectionBuilder, ProjectionBuilder>();
            services.AddScoped<IFormDataRepository, FormDataRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IFormDataService, FormDataService>();
            services.AddScoped<ICityService, CityService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
