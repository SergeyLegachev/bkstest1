﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using BksTest.Common;
using BksTest.Domain.Core;
using BksTest.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using BksTest.Web.Models;


namespace BksTest.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProjectionBuilder _projectionBuilder;
        private readonly IFormDataService _formDataService;
        private readonly ICityService _cityService;

        public HomeController(IProjectionBuilder projectionBuilder, IFormDataService formDataService, ICityService cityService)
        {
            _projectionBuilder = projectionBuilder;
            _formDataService = formDataService;
            _cityService = cityService;
        }

        public IActionResult Index()
        {
            IEnumerable<City> enumerable = _cityService.GetAll();
            return View(new FormDataViewModel(enumerable));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost("Create")]
        public async Task<ActionResult> Create(FormDataViewModel model)
        {
            FormData editTaskDto = _projectionBuilder.Build<FormDataViewModel, FormData>(model);
            await _formDataService.Create(editTaskDto);
            return RedirectToAction("Index", "Home");
        }
    }
}
