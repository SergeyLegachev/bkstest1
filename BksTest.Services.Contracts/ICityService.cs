﻿using System.Collections.Generic;
using BksTest.Domain.Core;

namespace BksTest.Services.Contracts
{
    public interface ICityService
    {
        IEnumerable<City> GetAll();
    }
}
