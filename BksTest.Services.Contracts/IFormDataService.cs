﻿using System.Threading.Tasks;
using BksTest.Domain.Core;

namespace BksTest.Services.Contracts
{
    public interface IFormDataService
    {
        Task Create(FormData editTaskDto);
    }
}
